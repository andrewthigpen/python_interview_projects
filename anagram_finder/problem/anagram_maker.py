#!/usr/bin/env python3
#
##########################
#      ANAGRAM MAKER
##########################
#
# Welcome to this coding challenge. A basic framework has been provided below.
# This is meant only to be a basic outline, feel free to modify, expand, remove,
# or change anything in this file. Good luck and don't forget to document your code.

import sys
from argparse import ArgumentParser, Namespace
from pathlib import Path
from typing import List, Union

CONST_ANAGRAMS = "anagrams"
CONST_FANCY = "fancy"
CONST_STATS = "stats"
CONST_WORD = "word"


def read_the_dictionary(dictionary_file: Union[str, Path]) -> List[str]:
    """Returns a list of lines from the given filename.

    The dictionary file is expected to contain one word per line.

    Args:
        dictionary_file (Union[str, Path]): filename of the dictionary

    Returns:
        List[str]: list of dictionary words
    """
    with open(dictionary_file, "r") as dictionary:
        return dictionary.readlines()


def find_all_the_anagrams(dictionary: List[str]):
    """Prints all the anagrams for words >= 4 characters.

    Args:
        dictionary (List[str]): list of dictionary words
    """
    print("All Anagrams:")
    raise NotImplementedError()


def most_anagrams(dictionary: List[str]):
    """Prints the longest list of anagrams and length of that list.

    Args:
        dictionary (List[str]): list of dictionary words
    """
    print("Most anagrams: ")
    print("Total amount: ")
    raise NotImplementedError()


def is_anagram(word: str, dictionary: List[str]):
    """Prints the anagrams for a specific word.

    Args:
        word (str): word
        dictionary (List[str]): list of dictionary words
    """
    print(f"Is {word} an anagram?")
    print("Anagrams: ")
    raise NotImplementedError()


def print_stats(dictionary: List[str]):
    """Prints anagram stats for the dictionary of words.

    Args:
        dictionary (List[str]): dictionary
    """
    print("Total words: ")
    print("Total anagrams: ")
    print("Largest word that has an anagram: ")
    print("Percentage of words that have anagrams: ")
    print("Average length of a word that has an anagram: ")
    raise NotImplementedError()


def parse_args() -> Namespace:
    parser = ArgumentParser(description="Anagram maker")

    parser.add_argument(
        "-a",
        "--anagrams",
        dest="choice",
        action="store_const",
        const=CONST_ANAGRAMS,
        help="Find all the anagrams",
    )
    parser.add_argument(
        "-f",
        "--fancy",
        dest="choice",
        action="store_const",
        const=CONST_FANCY,
        help="Find the word with the most anagrams",
    )
    parser.add_argument(
        "-s",
        "--stats",
        dest="choice",
        action="store_const",
        const=CONST_STATS,
        help="Print stats on all the anagrams found",
    )
    parser.add_argument(
        "-w",
        "--word",
        dest=CONST_WORD,
        help="Pass in a word to check for anagrams",
    )

    args = parser.parse_args()
    if not args.choice and not args.word:
        parser.print_help()
        sys.exit(1)

    return args


def anagram_maker():
    print("### A NAG RAM! ###")

    args = parse_args()

    dictionary = read_the_dictionary("english_words.txt")

    if args.choice == CONST_ANAGRAMS:
        find_all_the_anagrams(dictionary)
    elif args.choice == CONST_FANCY:
        most_anagrams(dictionary)
    elif args.choice == CONST_STATS:
        print_stats(dictionary)
    elif args.word:
        is_anagram(args.word, dictionary)


if __name__ == "__main__":
    anagram_maker()
