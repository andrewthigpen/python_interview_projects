#!/usr/bin/env python3
#
##########################
#      ANAGRAM MAKER
##########################
#
# Welcome to this coding challenge. A basic framework has been provided below.
# This is meant only to be a basic outline, feel free to modify, expand, remove,
# or change anything in this file. Good luck and don't forget to document your code.

import sys
from argparse import ArgumentParser, Namespace
from collections import defaultdict
from pathlib import Path
from typing import Dict, Iterable, List, Tuple, Union

CONST_ANAGRAMS = "anagrams"
CONST_FANCY = "fancy"
CONST_STATS = "stats"
CONST_WORD = "word"


def read_the_dictionary(dictionary_file: Union[str, Path]) -> List[str]:
    """Returns a list of lines from the given filename.

    The dictionary file is expected to contain one word per line.

    Args:
        dictionary_file (Union[str, Path]): filename of the dictionary

    Returns:
        List[str]: list of dictionary words
    """
    with open(dictionary_file, "r") as dictionary:
        return dictionary.readlines()


def sort_characters(word: str) -> str:
    return "".join(sorted(word.strip().lower()))


def process_dictionary(
    dictionary: Iterable[str],
) -> Tuple[Dict[str, List[str]], int, int]:
    anagrams = defaultdict(list)
    total_words = 0
    total_words_with_anagrams = 0
    for word in dictionary:
        word = word.strip()
        if not word:
            continue
        sorted_word = sort_characters(word)
        anagrams[sorted_word].append(word)
        count = len(anagrams[sorted_word])
        if count > 1:
            total_words_with_anagrams += 1 if count > 2 else 2
        total_words += 1
    return (
        {k: v for k, v in anagrams.items() if len(v) > 1},
        total_words,
        total_words_with_anagrams,
    )


def find_all_the_anagrams(dictionary: List[str]):
    """Prints all the anagrams for words >= 4 characters.

    Args:
        dictionary (List[str]): list of dictionary words
    """
    long_words = (word for word in dictionary if len(word) >= 4)
    all_anagrams, _, _ = process_dictionary(long_words)
    print("All Anagrams:")
    for anagrams in all_anagrams.values():
        print(", ".join(anagrams))


def most_anagrams(dictionary: List[str]):
    """Prints the longest list of anagrams and length of that list.

    Args:
        dictionary (List[str]): list of dictionary words
    """
    long_words = (word for word in dictionary if len(word) >= 4)
    all_anagrams, _, _ = process_dictionary(long_words)
    longest_total = 0
    longest_anagrams = []
    for anagrams in all_anagrams.values():
        if len(anagrams) > longest_total:
            longest_anagrams = anagrams
            longest_total = len(anagrams)
    print("Most anagrams: ", ", ".join(longest_anagrams))
    print("Total amount:  ", longest_total)


def is_anagram(word: str, dictionary: List[str]):
    """Prints the anagrams for a specific word.

    Args:
        word (str): word
        dictionary (List[str]): list of dictionary words
    """
    all_anagrams, _, _ = process_dictionary(dictionary)
    sorted_word = sort_characters(word)
    anagrams = all_anagrams[sorted_word]
    yes_or_no = "yes" if len(anagrams) > 1 else "no"
    print(f"Is {word} an anagram?", yes_or_no)
    if yes_or_no == "yes":
        print("Anagrams: ", ", ".join(anagrams))


def average_anagram_length(all_anagrams: Dict[str, List[str]]) -> float:
    return sum(len(chars) for chars in all_anagrams.keys()) / len(all_anagrams)


def largest_anagram(all_anagrams: Dict[str, List[str]]):
    largest = sorted(all_anagrams.keys(), key=lambda k: len(k))[-1]
    return all_anagrams[largest]


def print_stats(dictionary: List[str]):
    """Prints anagram stats for the dictionary of words.

    Args:
        dictionary (List[str]): dictionary
    """
    all_anagrams, total_words, total_words_with_anagrams = process_dictionary(
        dictionary
    )
    longest = largest_anagram(all_anagrams)
    print("Total words: ", total_words)
    print("Total anagrams: ", total_words_with_anagrams)
    print("Largest word that has an anagram: ", ", ".join(longest))
    print(
        "Percentage of words that have anagrams: ",
        total_words_with_anagrams / total_words * 100.0,
    )
    print(
        "Average length of a word that has an anagram: ",
        average_anagram_length(all_anagrams),
    )


def parse_args() -> Namespace:
    parser = ArgumentParser(description="Anagram maker")

    parser.add_argument(
        "-a",
        "--anagrams",
        dest="choice",
        action="store_const",
        const=CONST_ANAGRAMS,
        help="Find all the anagrams",
    )
    parser.add_argument(
        "-f",
        "--fancy",
        dest="choice",
        action="store_const",
        const=CONST_FANCY,
        help="Find the word with the most anagrams",
    )
    parser.add_argument(
        "-s",
        "--stats",
        dest="choice",
        action="store_const",
        const=CONST_STATS,
        help="Print stats on all the anagrams found",
    )
    parser.add_argument(
        "-w",
        "--word",
        dest=CONST_WORD,
        help="Pass in a word to check for anagrams",
    )

    args = parser.parse_args()
    if not args.choice and not args.word:
        parser.print_help()
        sys.exit(1)

    return args


def anagram_maker():
    print("### A NAG RAM! ###")

    args = parse_args()

    dictionary = read_the_dictionary("english_words.txt")

    if args.choice == CONST_ANAGRAMS:
        find_all_the_anagrams(dictionary)
    elif args.choice == CONST_FANCY:
        most_anagrams(dictionary)
    elif args.choice == CONST_STATS:
        print_stats(dictionary)
    elif args.word:
        is_anagram(args.word, dictionary)


if __name__ == "__main__":
    anagram_maker()
